﻿using UnityEngine;

public class SC_DamageReceiver : MonoBehaviour, IEntity
{
    [SerializeField]
    private FloatVariable floatVariablePlayerHP;
    //This script will keep track of player HP
    public float playerHP;
    public SC_CharacterController playerController;
    public SC_WeaponManager weaponManager;

    public void Update()
    {
        Debug.Log(floatVariablePlayerHP.runtimeValue);
    }

    public void ApplyDamage(float points)
    {
        playerHP = floatVariablePlayerHP.runtimeValue;
        playerHP -= points;

        if (playerHP <= 0)
        {
            //Player is dead
            playerController.canMove = false;
            playerHP = 0;
            floatVariablePlayerHP.runtimeValue = playerHP;
        }
        floatVariablePlayerHP.runtimeValue = playerHP;
        // Debug.Log("Runtime Variable:" + floatVariablePlayerHP.runtimeValue);
    } 
}