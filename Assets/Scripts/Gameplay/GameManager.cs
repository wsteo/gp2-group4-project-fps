using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private int _numOfQuizRobberCaught = 0;

    public float NumOfQuizRobberCaught {get => _numOfQuizRobberCaught;}

    [SerializeField]
    private int _totalQuizRobber = 15;

    public float TotalQuizRobber {get => _totalQuizRobber;}

    [SerializeField]
    private int _levelIndex;

    [SerializeField]
    private AudioSource bgm;

    [SerializeField]
    private AudioSource victorySound;

    public float LevelIndex {get => _levelIndex;}

    GameObject[] levelCompleteObject;
    

    private void Start()
    {
        _numOfQuizRobberCaught = 0;
        levelCompleteObject = GameObject.FindGameObjectsWithTag("ShowLevelComplete");       
        HideLevelComplete();
    }

    public void ValidateExit()
    {
        if (_numOfQuizRobberCaught == 0)
        {
            Debug.Log("Fresh start!");
        }
        else if (_numOfQuizRobberCaught >= _totalQuizRobber)
        {
            //StartCoroutine(exitTimer());
            bgm.Stop();
            victorySound.Play();
            ShowLevelComplete();
            Debug.Log("Exit level");
        }
        else
        {
            Debug.Log("Cannot exit level");
        }
    }

    public void UpdateNumOfQuizRobberCaught()
    {
        _numOfQuizRobberCaught++;
    }

    IEnumerator exitTimer(float duration = 10f)
    {
        float countdown = duration;
        while (countdown > 0)
        {
            countdown--;
            Debug.Log("Countdown " + countdown);
            yield return new WaitForSecondsRealtime(1);
        }
        SceneManager.LoadScene(_levelIndex);
    }

    public void ShowLevelComplete()
    {
        foreach (GameObject g in levelCompleteObject)
        {
            g.SetActive(true);
        }
    }

    //hides objects with ShowGameOver tag
    public void HideLevelComplete()
    {
        foreach (GameObject g in levelCompleteObject)
        {
            g.SetActive(false);
        }
    }
}