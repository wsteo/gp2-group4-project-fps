using UnityEngine;

public class ExitArea : MonoBehaviour
{
    [SerializeField]
    private GameEvent OnExitCollide;

    private bool isFirstCollisionTrigger = true;

    private void OnTriggerEnter(Collider other)
    {
        if(isFirstCollisionTrigger)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                OnExitCollide.Raise();
                isFirstCollisionTrigger = false;
                Debug.Log("Enter exit area!");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!isFirstCollisionTrigger)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                isFirstCollisionTrigger = true;
                Debug.Log("Exit area!");
            }
        }
    }
}