using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreSystem : MonoBehaviour
{
    [SerializeField]
    private TMP_Text HUDScore;

    public int totalScore;

    [SerializeField]
    private IntVariable playerTotalScore;

    private void Start()
    {
        playerTotalScore.runtimeValue = 0;
        totalScore = playerTotalScore.InitialValue;
    }

    private void Update()
    {
        HUDScore.text = playerTotalScore.runtimeValue.ToString();
        //UpdateScore();
    }

    public void UpdateScore()
    {
        string totalScoreMessage = playerTotalScore.runtimeValue.ToString();
        HUDScore.text = totalScoreMessage;
        playerTotalScore.runtimeValue = totalScore;
    }
}