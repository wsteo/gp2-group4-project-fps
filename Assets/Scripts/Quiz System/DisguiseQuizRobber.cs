using UnityEngine;

public class DisguiseQuizRobber : MonoBehaviour
{
    [SerializeField]
    private GameObject quizRobberPrefab;

    private float locationX;
    private float locationY;
    private float locationZ;

    private float rotationX;
    private float rotationY;
    private float rotationZ;

    public void GotDetected()
    {
        SetLocationAndRotation();

        Vector3 QuizRobberSpawnLocation = new Vector3(locationX, locationY, locationZ);

        Quaternion QuizRobberSpawnRotation = new Quaternion();
        QuizRobberSpawnRotation.Set(rotationX,rotationY,rotationZ,1);

        Instantiate(quizRobberPrefab, QuizRobberSpawnLocation, QuizRobberSpawnRotation);
        Destroy(this.gameObject);
        Debug.Log("Got discovered");
    }

    void SetLocationAndRotation()
    {
        locationX = this.gameObject.transform.localPosition.x;
        locationY = this.gameObject.transform.localPosition.y;
        locationZ = this.gameObject.transform.localPosition.z;
        rotationX = this.gameObject.transform.localRotation.x;
        rotationY = this.gameObject.transform.localRotation.y;
        rotationZ = this.gameObject.transform.localRotation.z;
    }
}