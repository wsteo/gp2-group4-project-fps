using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuizManager : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;
    
    [SerializeField]
    private SC_CharacterController playerController;

    [SerializeField]
    private IntVariable totalScore;

    [SerializeField]
    private GameObject quizUI;

    [SerializeField]
    private Image questionImageMini;

    [SerializeField]
    private Image questionImageFull;

    [SerializeField]
    private TMP_Text questionDescription;

    [Header("Answer Option (Button Group)")]
    [SerializeField]
    private TMP_Text buttonTextOptionA;

    [SerializeField]
    private TMP_Text buttonTextOptionB;

    [SerializeField]
    private TMP_Text buttonTextOptionC;

    [SerializeField]
    private TMP_Text buttonTextOptionD;

    [SerializeField]
    private List<QuestionData> questions;

    private int answer;

    private int points;

    private int index;
    public void SetQuestion()
    {
        int questionIndex = index % questions.Count;
        QuestionData questionInformation = questions[questionIndex];
        Vector2 imageDimension = new Vector2(questionInformation.questionImageWidth,questionInformation.questionImageHeight);
        Vector3 imageScale = new Vector3(questionInformation.questionImageScale,questionInformation.questionImageScale,questionInformation.questionImageScale);
        quizUI.SetActive(true);
        questionImageMini.sprite = questionInformation.questionImage;
        questionImageFull.sprite = questionInformation.questionImage;
        questionImageMini.rectTransform.sizeDelta = imageDimension;
        questionImageFull.rectTransform.sizeDelta = imageDimension;
        questionImageFull.rectTransform.localScale = imageScale;
        questionDescription.text = questionInformation.description;
        buttonTextOptionA.text = questionInformation.options[0];
        buttonTextOptionB.text = questionInformation.options[1];
        buttonTextOptionC.text = questionInformation.options[2];
        buttonTextOptionD.text = questionInformation.options[3];
        answer = questionInformation.answer;
        points = questionInformation.points;
        PauseScene();
        index++;
    }

    public void ValidateAnswer(IntVariable userAnswer)
    {
        if (userAnswer.runtimeValue == answer)
        {
            totalScore.runtimeValue += points;
            gameManager.UpdateNumOfQuizRobberCaught();
            quizUI.SetActive(false);
            Debug.Log("Correct!");
        }
        else
        {
            Debug.Log("Wrong!");
            quizUI.SetActive(false);
        }
        ResumeScene();
    }

    public void PauseScene()
    {
        Time.timeScale = 0;
        playerController.enabled = false;
        Debug.Log("Pause Scene!");
    }

    public void ResumeScene()
    {
        Time.timeScale = 1;
        playerController.enabled = true;
        Debug.Log("Play Scene!");
    }
}
