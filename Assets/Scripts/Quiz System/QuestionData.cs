using UnityEngine;

[CreateAssetMenu(fileName = "New Question", menuName = "Quiz System/Question Data", order = 0)]
public class QuestionData : ScriptableObject
{
    public int id;
    public Sprite questionImage;

    public float questionImageWidth;

    public float questionImageHeight;

    public float questionImageScale = 1f;

    [TextArea(10, 30)]
    public string description;

    [TextArea(5, 10)]
    public string[] options;

    public int answer = 0;

    public int points = 5;
}