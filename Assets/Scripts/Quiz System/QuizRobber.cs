using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizRobber : MonoBehaviour
{
    [SerializeField]
    private GameEvent OnQuizRobberCollide;
    
    private bool isFirstCollisionTrigger = true;
    
    private void OnTriggerEnter(Collider other)
    {
        if(isFirstCollisionTrigger)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                OnQuizRobberCollide.Raise();
                isFirstCollisionTrigger = false;
                Destroy(this.gameObject);
                Debug.Log("Collision triggered!");
            }
        }
    }
}
