using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New IntVariable", menuName = "Scriptable Object Variables/IntVariable", order = 0)]
public class IntVariable : ScriptableObject, ISerializationCallbackReceiver
{
    [SerializeField]
    private int _initialValue;

    public int InitialValue { get => _initialValue; }

    [NonSerialized]
    public int runtimeValue;

    public void OnAfterDeserialize()
    {
        runtimeValue = _initialValue;
    }

    public void OnBeforeSerialize() { }
}