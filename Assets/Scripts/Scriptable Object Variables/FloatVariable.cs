using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New FloatVariable", menuName = "Scriptable Object Variables/FloatVariable", order = 0)]
public class FloatVariable : ScriptableObject, ISerializationCallbackReceiver
{
    [SerializeField]
    private float _initialValue;

    public float InitialValue { get => _initialValue; }

    [NonSerialized]
    public float runtimeValue;

    public void OnAfterDeserialize()
    {
        runtimeValue = _initialValue;
    }

    public void OnBeforeSerialize() { }
}