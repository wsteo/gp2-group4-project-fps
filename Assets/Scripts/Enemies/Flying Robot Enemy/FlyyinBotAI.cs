using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FlyyinBotAI : MonoBehaviour
{
    public enum State
    {
        Roaming,
        ChaseTarget,
        AttackTarget,
        GoingToStart,
    }

    private NavMeshAgent agent;

    public FloatVariable playerHp;
    public HealthBar barHP;
    public AudioSource audioAlert;

    public float radius;
    public Transform player;
    private State state;
    bool isDone= false;

    int test;
    
    private void Awake()
    {
        state= State.Roaming;
    }

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        switch (state)
        {
            default:
            case State.Roaming:
                Roaming();
                break;

            case State.ChaseTarget:
                ChaseTarget();                
                break;

            case State.AttackTarget:
                Attack();
                break;

            case State.GoingToStart:
                GoToStart();
                break;
        }
        
    }
    
    //ROAMING
    private void Roaming()
    {
        if (!agent.hasPath)
        {
            agent.SetDestination(GetPoint.Instance.GetRandomPoint());
        }

        FindTarget();
    }

    //FIND TARGET
    private void FindTarget()
    {
        float targetRange = 10f;
        if(Vector3.Distance(GetPoint.Instance.GetPointPosition(), player.transform.position)< targetRange)
        {
            state = State.ChaseTarget;
        }
    }

    //CHASE TARGET
    private void ChaseTarget()
    {
        agent.SetDestination(player.position);
        transform.LookAt(player);
        
        if (Vector3.Distance(transform.position , player.transform.position) < 2f)
        {
            state = State.AttackTarget;
        }
        
        float stopChaseRange = 10f;
        if (Vector3.Distance(GetPoint.Instance.GetPointPosition(), player.transform.position) > stopChaseRange)
        {
            state = State.Roaming;          
        }    
    }

    private void Attack()
    {
        
        if (!isDone)
        {      
            playerHp.runtimeValue = playerHp.runtimeValue - 25;
            test += 1;
            Debug.Log(playerHp.runtimeValue);   
            Debug.Log(test);
            isDone = true;
        }

        barHP.SetHealth();
        state = State.GoingToStart;

    }


    private void GoToStart()
    {

        agent.SetDestination(GetPoint.Instance.GetPointPosition());
        StartCoroutine(DelayStateLoad());
        isDone = false;
    }

    IEnumerator DelayStateLoad()
    {
        yield return new WaitForSeconds(8);
        state = State.Roaming;
    }

    //Unity Editor
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
