using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField]
    private float speed;

    public Vector3 Direction { get; set; }

    public float damage = 0f;

    public FloatVariable playerHP;


    // Update is called once per frame
    void Update()
    {
        transform.Translate(Direction * speed * Time.deltaTime, Space.World);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        playerHP.runtimeValue -= damage;
        //Destroy(gameObject);
    }
}
