using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Scorpion : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;
    private NavMeshPath path;
    public float timerForNewPath;
    private bool inCoRoutine;
    private Vector3 target;
    
    [SerializeField]
    private float damageToPlayer = 5;

    [SerializeField]
    private FloatVariable playerHP;

    private int collisionCounter = 0;

    [SerializeField]
    private int numOfCollisionToDestroy = 5;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        path = new NavMeshPath();
    }

    private void Update()
    {
        if(!inCoRoutine)
        {
            StartCoroutine(DoSomething());
        }

    }

    public Vector3 GetNewRandomPosition()
    {
        float x = Random.Range(-20, 20);
        float z = Random.Range(-20, 20);

        Vector3 pos = new Vector3(x, 0, z);
        return pos;
    }

    IEnumerator DoSomething()
    {
        inCoRoutine = true;
        yield return new WaitForSeconds(timerForNewPath);
        GetNewPath();
        if(!navMeshAgent.CalculatePath(target,path))
        {
            Debug.Log("Find an invalid path.");
        }
        inCoRoutine = false;
    }

    public void GetNewPath()
    {
        target = GetNewRandomPosition();
        navMeshAgent.SetDestination(target);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerHP.runtimeValue -= damageToPlayer;
            Debug.Log("Attack player!");
            collisionCounter++;
            Debug.Log(collisionCounter);

            if (collisionCounter >= numOfCollisionToDestroy)
            {
                Destroy(this.gameObject);
            }
        }
    }
}