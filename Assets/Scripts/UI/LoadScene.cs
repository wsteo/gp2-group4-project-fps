using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    
    public int index;
    public AudioSource music;

    public void LoadLevel()
    {
        SceneManager.LoadScene(index);
    }

    public void DelayedLoad()
    {
        StartCoroutine(DelaySceneLoad(index));
    }

    public void Exitgame()
    {
        Application.Quit();
    }

    public void Reload()
    {
        StartCoroutine(DelaySceneLoad(index));
    }

    public void BackMainMenu()
    {
        StartCoroutine(DelaySceneLoad(0));
    }

    IEnumerator DelaySceneLoad(int indexLevel)
    {
        yield return new WaitForSeconds(music.clip.length);
        SceneManager.LoadScene(indexLevel);
    }
}
