using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverLevel : MonoBehaviour
{
	GameObject[] gameoverObjects;
	public FloatVariable playerHP;

	// Use this for initialization
	void Start()
	{
		Time.timeScale = 1;
		playerHP.runtimeValue = playerHP.InitialValue;
		gameoverObjects = GameObject.FindGameObjectsWithTag("ShowGameOver");
		HideGameOver();
	}

	// Update is called once per frame
	void Update()
	{	
		if(playerHP.runtimeValue <= 0)
		{
			Time.timeScale = 0;
			ShowGameOver();
		}
	}


	//shows objects with ShowGameOver tag
	public void ShowGameOver()
	{
		foreach (GameObject g in gameoverObjects)
		{
			g.SetActive(true);
		}
	}

	//hides objects with ShowGameOver tag
	public void HideGameOver()
	{
		foreach (GameObject g in gameoverObjects)
		{
			g.SetActive(false);
		}
	}
}
