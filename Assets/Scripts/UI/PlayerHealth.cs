using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{

    public FloatVariable playerHP;
    public HealthBar healthbar;
    public float damageValue;

    void Start()
    {
        healthbar.SetMaxHealth();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            DamagePlayer();
        }
    }

    public void DamagePlayer()
    {
        playerHP.runtimeValue -= damageValue;

        healthbar.SetHealth();
    }
}
