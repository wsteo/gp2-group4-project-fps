using System.Collections;
using UnityEngine;
using TMPro;

public class CountdownTimer : MonoBehaviour
{
    [SerializeField]
    private TMP_Text timeRemaining;
    public FloatVariable timer;

    public void StartTimer()
    {
        timer.runtimeValue = timer.InitialValue;
        StartCoroutine(CountTimer());
    }

    public void StopTimer()
    {
        StopAllCoroutines();
    }

    IEnumerator CountTimer()
    {
        while (timer.runtimeValue > 0)
        {
            timer.runtimeValue -= Time.unscaledDeltaTime;
            DisplayTime(timer.runtimeValue);
            yield return null;
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        string timerString = string.Format("{0:00}:{1:00}", minutes, seconds);
        timeRemaining.text = timerString;
    }
}