using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider;
    public Gradient gradient;
    public Image fill;

    public GameObject gameOver;

    public FloatVariable playerHP;

    private void Start()
    {
        SetMaxHealth();
    }

    void Update()
    {
        SetHealth();
    }

    public void SetMaxHealth()
    {
        slider.maxValue = playerHP.InitialValue;
        slider.value = playerHP.InitialValue;

        fill.color = gradient.Evaluate(1f);
    }

    public void SetHealth()
    {
        slider.value = playerHP.runtimeValue;

        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
